<?php
/*
Plugin Name:    Base Functions Plugin
Plugin URI:     https://tmprs.net
Description:	Code base for a site specific WordPress functionality plugin. Must be customized obviously
Author:       	aris~
Author URI:     https://codeberg.org/tmprs/
License:        GNU General Public License v2 or later
License URI:    http://www.gnu.org/licenses/gpl-2.0.txt
Version:		2022.11.27
*/

if ( ! defined( 'ABSPATH' ) ) { die; }

/**
 **      WordPress Core Functions
 **      -------------------------------------
 **/

/*
 * Define or disable post revisions
 * https://wordpress.org/support/article/revisions/
 * http://wp-functions.com/wp-admin-functions/limit-wordpress-post-revisions/
 * if (!defined('WP_POST_REVISIONS')) define('WP_POST_REVISIONS', 5);
 * Note: it’s better to call this in wp-config.php
 */
if (!defined('WP_POST_REVISIONS')) define('WP_POST_REVISIONS', false);

/*
 * Define autosave in seconds. 
 * Exemple: 86400 for one day... i.e. never
 μ Note: it’s better to call this in wp-config.php
 */
if (!defined('AUTOSAVE_INTERVAL')) define('AUTOSAVE_INTERVAL', 86400);

/*
 * WordPress Headers 
 * ------------------------------------- 
 */

// https://www.wpbeginner.com/wp-tutorials/the-right-way-to-remove-wordpress-version-number/
add_filter('the_generator', 'tmprs_remove_version');
function tmprs_remove_version() {return '';}
add_filter('style_loader_src', 'tmprs_remove_version_scripts_styles', 9999);
add_filter('script_loader_src', 'tmprs_remove_version_scripts_styles', 9999);
function tmprs_remove_version_scripts_styles($src){if(strpos($src,'ver=')){$src=remove_query_arg('ver',$src);}return $src;}

// https://crunchify.com/how-to-clean-up-wordpress-header-section-without-any-plugin/
// https://geekflare.com/wordpress-performance-optimization-without-plugin/
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'rest_output_link_wp_head', 10);
remove_action('template_redirect', 'rest_output_link_header', 11, 0);
//remove_action('wp_head', 'wp_oembed_add_discovery_links', 10); // NEED INVESTIGATION

/*
 *      Global site functions support
 *      ------------------------------------- 
 */

/*
 * Disable Emojis 
 * https://www.denisbouquet.com/remove-wordpress-emoji-code/
 */
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');
add_action('wp_enqueue_scripts', 'tmprs_deregister_jquery');

/*
 * Deregister JQuery for Public Site
 * Note : Maybe move this to Theme’s function file
 * https://geekflare.com/wordpress-performance-optimization-without-plugin/
 */
function tmprs_deregister_jquery() { if ( !is_admin() ) {wp_deregister_script('jquery');} } 

/*
 * Remove post format support
 * Note : Maybe move this to Theme’s function file
 * https://wordpress.stackexchange.com/a/65654
 */
add_action('after_setup_theme', 'tmprs_remove_post_format', 15);
function tmprs_remove_post_format() { remove_theme_support('post-formats'); }

/*
 * Wordpress to WordPress auto correction
 */
remove_filter ( 'the_title', 'capital_P_dangit', 11 );
remove_filter ( 'the_content', 'capital_P_dangit', 11 );
remove_filter ( 'comment_text', 'capital_P_dangit', 31 );

/*
 * Disable WebP By Default - 0.2
 * https://wordpress.org/plugins/disable-webp-by-default/
 * For WordPress 6.1
add_filter( 'wp_upload_image_mime_transforms', '__return_empty_array' );
 */
 
/*
 * Limit Heartbeat API
 * Note: Experimental - NEED REVIEW
 * https://www.wpbeginner.com/plugins/how-to-limit-heartbeat-api-in-wordpress/
 *
add_action( 'init', 'stop_heartbeat', 1 );
function stop_heartbeat() { wp_deregister_script('heartbeat'); }
 *
 */

/*
 *      Content
 *      ------------------------------------- 
 */
 
 /* 
  * Disable Media Permalink - 1.0
  * https://wordpress.org/plugins/disable-media-permalink-by-hardweb-it/
  */
add_filter( 'rewrite_rules_array', 'tmprs_cleanup_attachment_permalink' );
function tmprs_cleanup_attachment_permalink( $rules ) {
	foreach ( $rules as $regex => $query ) {
		if ( strpos( $regex, 'attachment' ) || strpos( $query, 'attachment' ) ) {
			unset( $rules[ $regex ] );
		}
	}
	return $rules;
}
add_filter( 'attachment_link', 'tmprs_cleanup_attachment_link' );
function tmprs_cleanup_attachment_link( $link ) {
	return;
}
 
 /*
  * Disable Author Archives - 1.3.1
  * https://wordpress.org/plugins/disable-author-archives
  */
add_action('template_redirect',function(){if(isset($_GET['author'])||is_author()){global $wp_query;$wp_query->set_404();status_header(404);nocache_headers();}},1);
add_filter('user_row_actions',function($actions){if(isset($actions['view']))unset($actions['view']);return $actions;},PHP_INT_MAX,2);
add_filter( 'author_link', function() { return '#'; }, PHP_INT_MAX );
add_filter( 'the_author_posts_link', '__return_empty_string', PHP_INT_MAX );

/*
 * Display Featured Images in RSS Feed 
 * Source: Newspack Theme
 */
add_filter( 'the_excerpt_rss', 'tmprs_thumbnails_in_rss' );
add_filter( 'the_content_feed', 'tmprs_thumbnails_in_rss' );
function tmprs_thumbnails_in_rss( $content ) {
	global $post;
	if ( has_post_thumbnail( $post->ID ) ) {
		$content = '<figure>' . get_the_post_thumbnail( $post->ID, 'medium' ) . '</figure>' . $content;
	}
	return $content;
}

/*
 * Publish later on feed
 * https://www.wpbeginner.com/
 */
add_filter('posts_where', 'publish_later_on_feed');
function publish_later_on_feed($where) {
    global $wpdb;
    if ( is_feed() ) {
        // timestamp in WP-format
        $now = gmdate('Y-m-d H:i:s');
        // value for wait; + device
        $wait = '10'; // integer
        // http://dev.mysql.com/doc/refman/5.0/en/date-and-time-functions.html#function_timestampdiff
        $device = 'MINUTE'; //MINUTE, HOUR, DAY, WEEK, MONTH, YEAR	
        // add SQL-sytax to default $where
        $where .= " AND TIMESTAMPDIFF($device, $wpdb->posts.post_date_gmt, '$now') > $wait ";
    }
    return $where;
}


/*
 * Disable all RSS Feeds
 * https://kinsta.com/fr/base-de-connaissances/desactiver-flux-rss-wordpress/
 *
add_action('do_feed', 'tmprs_disable_feed', 1);
add_action('do_feed_rdf', 'tmprs_disable_feed', 1);
add_action('do_feed_rss', 'tmprs_disable_feed', 1);
add_action('do_feed_rss2', 'tmprs_disable_feed', 1);
add_action('do_feed_atom', 'tmprs_disable_feed', 1);
add_action('do_feed_rss2_comments', 'tmprs_disable_feed', 1);
add_action('do_feed_atom_comments', 'tmprs_disable_feed', 1);
function tmprs_disable_feed() {
   wp_die( __( 'No feed available, please visit the <a href="'. esc_url( home_url( '/' ) ) .'">homepage</a>!' ) );
}
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );
 *
 */

/*
 *      Admin UI
 *      ------------------------------------- 
 */

add_filter( 'show_admin_bar', '__return_false' );
remove_action('welcome_panel', 'wp_welcome_panel');

add_action( 'admin_init', 'tmprs_remove_dashboard_meta' ); 
function tmprs_remove_dashboard_meta() {
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	//remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
	//remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	//remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
}

/*
 * Custom admin footer 
 * https://www.elegantthemes.com/blog/tips-tricks/17-wordpress-functions-php-file-hacks
 */
add_filter('admin_footer_text', 'tmprs_change_footer_admin');
function tmprs_change_footer_admin () { echo 'Welcome to Your WordPress'; }

/*
 * Branding
 * https://wphelp.blog/how-to-remove-or-change-the-logo-from-the-wordpress-admin-bar/
 */
add_action( 'wp_before_admin_bar_render', 'tmprs_remove_logo_wp_admin', 0 );
function tmprs_remove_logo_wp_admin() { global $wp_admin_bar; $wp_admin_bar->remove_menu( 'wp-logo' ); }

/*
 * Remove screen option
 * Is this really usefull?
 *
add_filter('screen_options_show_screen', 'tmprs_remove_screen_options');
function tmprs_remove_screen_options() { if(!current_user_can('manage_options')) { return false;} return true; }
 *
 */

/*
 *      Admin Functions
 *      ------------------------------------- 
 */

/*
 * Disable user password change mail notification
 * Note : Read the tutorial to understand !
 * https://wpchannel.com/wordpress/tutoriels-wordpress/desactiver-notifications-wordpress/
 */
add_filter('send_password_change_email', '__return_false');
remove_action('after_password_reset', 'wp_password_change_notification');
// Disable admin password change notification by mail
add_filter( 'admin_email_check_interval', '__return_false' );
// Disable updates notification by mail
add_filter('auto_plugin_update_send_email', '__return_false');
add_filter('auto_theme_update_send_email', '__return_false');

/*
 *      Security & Spam
 *      ------------------------------------- 
 */

// https://www.wpbeginner.com/plugins/how-to-disable-xml-rpc-in-wordpress/
add_filter('xmlrpc_enabled', '__return_false'); // Warning -> WP desktop Apps will not work
add_filter('wp_is_application_passwords_available', '__return_false');  // Warning -> WP Mobiles Apps will not work

// https://www.wpbeginner.com/wp-tutorials/how-to-disable-login-hints-in-wordpress-login-error-messages/
add_filter( 'login_errors', 'tmprs_no_wordpress_errors' );
function tmprs_no_wordpress_errors(){ return 'Houston, nous avons un problème...'; }

// https://wordpress.org/plugins/simply-disable-password-reset/
function tmprs_disable_password_reset() { return false; }
add_filter ( 'allow_password_reset', 'tmprs_disable_password_reset' );

// https://www.wpbeginner.com/wp-tutorials/how-disable-self-pingbacks-in-wordpress/
add_action('pre_ping','tmprs_no_self_ping');
function tmprs_no_self_ping(&$links){$home=get_option('home');foreach($links as $l=>$link)if(0===strpos($link,$home))unset($links[$l]);}

// https://perishablepress.com/stop-user-enumeration-wordpress/
if (!is_admin()) {
	if (preg_match('/author=([0-9]*)/i', $_SERVER['QUERY_STRING'])) die('No Way!');
	add_filter('redirect_canonical', 'tmprs_check_enum', 10, 2);
}
function tmprs_check_enum($redirect, $request) {
	if (preg_match('/\?author=([0-9]*)(\/*)/i', $request)) die();
	else return $redirect;
}

/*
 *      Plugins
 *      Add here actions, filters and functions related to plugins
 *      ------------------------------------- 
 */

/*
 * Sample - Share on Mastodon - Tested with 0.6.4
 *
add_filter( 'share_on_mastodon_cutoff', '__return_true' );
add_filter( 'share_on_mastodon_attached_images', '__return_false' );
add_filter( 'share_on_mastodon_status', function( $status, $post ) {
    $status  = get_the_title( $post ) . "\n\n";
    //$status .= apply_filters( 'the_excerpt', get_the_excerpt( $post ) ) . "\n\n";
    $status .= get_permalink( $post );
    $status = wp_strip_all_tags(
        html_entity_decode( $status, ENT_QUOTES | ENT_HTML5, get_bloginfo( 'charset' ) )
    );
    return $status;
}, 10, 2 );
 *
 */